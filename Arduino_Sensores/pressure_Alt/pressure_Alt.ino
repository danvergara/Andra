#include <DHT11.h> //Sensor DHT11
#include <SPI.h>
//NRF24L01
#include <RF24.h>
//Líbrerías necesarias para el sensor BMP180
#include <SFE_BMP180.h>
#include <Wire.h>

//We will need to create an SFE_BMP180 object, here called "pressure"
SFE_BMP180 pressure;

//Variables to compute:
// 1.- The current altitud
// 2.- The absolute pressure
// 3.- The relative (sea-level) compensated pressure in mb

//Firt, define the sea level pressure as a constant 
#define p0  1013.25  //Sea level Pression (hPa) 

//Declaramos los pines CE y el CSN
#define CE_PIN 9
#define CSN_PIN 10

//Declaramos el pin al que irá conectado el sensor DTH11
#define PIN_DHT11 4

//Declare pin for the MQ135 (CO2) sensor
const int mq135 = 0;
float mq135Float = 0;

RF24 radio(CE_PIN, CSN_PIN);

//Instanciamos un objeto de la clase DHT11 y le pasamos el pin correspondiente en el constructor
DHT11 dht11(PIN_DHT11);

int wait = 100;
//Vector con los datos a enviar
float datosClima[2];

//String used to stored the total data as a package
String totalPayLoad = "";

//String variables used to store the sent data
char SendPayloadTemp[10] = "";
char SendPayloadHum[10] = "";
char SendPayloadPress[10] = "";
char SendPayloadAlt[10] = "";
char SendPayloadSeaLevel[10] = "";
char SendPayloadCO2[10]  = "";

char pipe1[8] ="0000001";
char pipe2[8] ="0000002";
char pipe3[8] ="0000003";
char pipe4[8] ="0000004";
char pipe5[8] ="0000005"; 

void setup()
{
   radio.begin();
   Serial.begin(9600);
   Serial.println("REBOOT");
   radio.setPALevel(RF24_PA_MAX);
   radio.setChannel(0x76);
   radio.openWritingPipe(0xF0F0F0F0E1);
   radio.enableDynamicPayloads();
   radio.powerUp();

   // Initialize the sensor (it is important to get calibration values stored on the device).
  if (pressure.begin())
    Serial.println("BMP180 init success");
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    Serial.println("BMP180 init fail\n\n");
    while(1); // Pause forever.
  }
}

void loop()
{
  //Declare variables for Pressure measurement
  double currentAlt, absolutePress, seaLevelPress;
  //This variables are used to store the new values converted to float numbers from the variables above
  float currentAltFloat, absolutePressFloat, seaLevelPressFloat;
  int err; //Variable used to validate the measurement 
  float temp, hum; //variable which stored the temperature and the humidity
  if((err = dht11.read(hum, temp)) == 0)    // Si devuelve 0 es que ha leido bien
  {
    //Convertimos la temperatura y la humedad de float a String
    dtostrf(temp,2,2,SendPayloadTemp);
    dtostrf(hum,2,2,SendPayloadHum);
    
    //Call the getPressure function
    absolutePress = getPressure();
    //Convert the abasolutePress double value to float value
    absolutePressFloat = float(absolutePress);
    //Convert the float number to String
    dtostrf(absolutePressFloat,2,2,SendPayloadPress);


    //The pressure sensor returns the current altitude. Determine the altitud from the abosulte pressue reading.
    //Use the altitude function along with a baseline pressure (sea-level or other).
    // Parameters: absolutePress = absolute pressure in mb, p0 = sea-level pressure in mb.
    // Result: currentAlt = altitude in m.
  
    currentAlt =  pressure.altitude(absolutePress, p0);
    //Convert the currentAlt double value to float value
    currentAltFloat = float(currentAlt);
    //Convert the float number to String
    dtostrf(currentAltFloat,2,2,SendPayloadAlt);

     // The pressure sensor returns abolute pressure, which varies with altitude.
    // To remove the effects of altitude, use the sealevel function and your current altitude.
    // This number is commonly used in weather reports.
    // Parameters: absolutePress = absolute pressure in mb, currentAlt = current altitude in m.
    // Result: seaLevelPress = sea-level compensated pressure in mb
    
    seaLevelPress = pressure.sealevel(absolutePress, currentAlt);
     //Convert the currentAlt double value to float value
    seaLevelPressFloat = float(seaLevelPress);
    //Convert the float number to String
    dtostrf(seaLevelPressFloat,2,2,SendPayloadSeaLevel);

    //get the volume of the CO2 in the air
    mq135Float = CO2();
    //Convert the float number to String
    dtostrf(mq135Float,2,2,SendPayloadCO2);
   

    //Concatenamos el string resultante con un tag name para saber a que magnitud corresponde el dato
    strcat(SendPayloadTemp, "T");
    strcat(SendPayloadHum, "H");
    strcat(SendPayloadPress, "P");
    strcat(SendPayloadAlt, "M");
    strcat(SendPayloadSeaLevel, "S");
    strcat(SendPayloadCO2, "C");
    
    //Enviamos los datos a la Raspberry Pi  
    bool okTemp = radio.write(&SendPayloadTemp,sizeof(SendPayloadTemp));
    delay(wait);
    bool okHum = radio.write(&SendPayloadHum,sizeof(SendPayloadHum));
    delay(wait);
    bool okPress = radio.write(&SendPayloadPress,sizeof(SendPayloadPress));
    delay(wait);
    bool okAlt = radio.write(&SendPayloadAlt,sizeof(SendPayloadAlt));
    delay(wait);
    bool okSea = radio.write(&SendPayloadSeaLevel,sizeof(SendPayloadSeaLevel));
    delay(wait);
    bool okCO2 = radio.write(&SendPayloadCO2,sizeof(SendPayloadCO2));
    delay(wait);
    //Si los datos fueron enviados correctamente, lo mostramos en consola
    if(okTemp && okHum && okPress && okAlt && okSea && okCO2)
    {
      Serial.println(totalPayLoad);
      Serial.print("Temperatura: ");
      Serial.print(temp);
      Serial.print(" °");
      Serial.print(" Humedad: ");
      Serial.print(hum);
      Serial.print(" %");
      Serial.println();
      Serial.print("Absolute pressure: ");
      Serial.print(absolutePress);
      Serial.print(" hPa, ");
      Serial.print(absolutePress*0.0295333727,2);
      Serial.println(" inHg");
      Serial.println();
      Serial.print("computed altitude: ");
      Serial.print(currentAlt);
      Serial.print(" meters, ");
      Serial.print(currentAlt*3.28084,0);
      Serial.println(" feet");
      Serial.println(); 
      Serial.print("relative (sea-level) pressure: ");
      Serial.print(seaLevelPress,2);
      Serial.print(" hPa, ");
      Serial.print(seaLevelPress*0.0295333727,2);
      Serial.println(" inHg");
      Serial.println(); 
      Serial.println("*************************************");
      Serial.println();
    } 
    else
    {
      Serial.println("No se ha podido enviar");
    }   
  }
  else
  {
     Serial.println();
     Serial.print("Error Num :");
     Serial.print(err);
     Serial.println();
  }
  
  delay(1000);            //Recordad que solo lee una vez por segundo
}

double getPressure()
{
  char status;
  double T,P;

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:

    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T);
        if (status != 0)
        {
          return(P);
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}



float CO2 ()
{
  float sensor = analogRead(mq135);
  float ppm1 = (sensor/41763);
  float ppm2 = pow(ppm1,-2.769034857);
  float ppm3 = 116.6020682*ppm2;
  float ppm4 = ppm3/10000;
  return(ppm4);  
}


