#!/usr/bin/env python
# -*- coding: UTF-8 -*-a
import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import time
from datetime import datetime


class Main(QMainWindow):

    def __init__(self, parent= None):
        QMainWindow.__init__(self, parent)
        self.InitUi()
        self.autoState = False

    def Toolbar(self):
        #Exit Action
        self.statusBar()
        self.exitAction = QAction(QIcon('Button-Close-icon.png'), 'Cerrar', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.setStatusTip('Salir de la aplicación')
        self.exitAction.triggered.connect(self.close)

        #Help Action
        self.helpAction = QAction(QIcon('helpIcon.png'), 'Ayuda', self)
        self.helpAction.setShortcut('Ctrl+H')
        self.helpAction.setStatusTip('Ayuda')
        #self.helpAction.triggered.connect(self.helpfile)

        #Back Action
        self.backAction = QAction(QIcon('arrow-back-icon.png'), 'Regresar', self)
        self.backAction.setShortcut('Ctrl+B')
        self.backAction.setStatusTip('Regresar al menú')
        #self.backAction.triggered.connect(self.toMenu)


        toolbar = self.addToolBar('Cerrar')
        toolbar.addAction(self.exitAction)
        #backBar = self.addToolBar('Regresar')
        #backBar.addAction(self.backAction)

    def tabWid(self):
        #instancia del objeto QTabWidget
        self.tabs = QTabWidget()

        #Instancias del objeto QWidget que contendrán las vistas
        self.foreTab = QWidget()
        self.autoTab = QWidget()
        self.conTab = QWidget()
        self.tabs.addTab(self.foreTab, "Predicción")
        self.tabs.addTab(self.autoTab, "Vuelo Autónomo")
        self.tabs.addTab(self.conTab, "Vuelo Controlado")

    def forecastTable(self):
        gridFore = QGridLayout()
        today = datetime.today()
        hoy = datetime.weekday(today)
        dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]

        names = ['', dias[hoy], dias[hoy+1], dias[hoy+2], dias[hoy+3], dias[hoy+4],
                 'Temperatura', '24 °', '29 °', '27 °', '25 °', '24 °',
                 'Humedad', '33 %', '34 %', '31 %', '38 %', '34 %',
                ]
        positions = [(i,j) for i in range(3) for j in range(6)]

        for position, name in zip(positions, names):
            if name == '':
                continue
            letrero = QLabel()
            letrero.setText(name)
            letrero.setStyleSheet('font-size: 24px;')
            gridFore.addWidget(letrero, *position)
        self.foreTab.setLayout(gridFore)


    def controlTab(self):
        gridLay = QGridLayout()

        imagenes = ['','image','']

        positions = [(i,j) for i in range(1) for j in range(3)]

        for position, imagen in zip(positions, imagenes):
            #if imagen == '':
                #continue
            #btnImage= QPushButton(imagen)
            if imagen == '':
                #btnImage = QPushButton(imagen)
                btnImage = QLabel(self)
                btnImage.setText('')
            if imagen == 'image':
                #btnImage = QLabel(self)
                #btnImage.setText('nothing')
                btnImage = QLabel(self)
                pixmap = QPixmap('xboxController.png')
                btnImage.setPixmap(pixmap)
            gridLay.addWidget(btnImage, *position)

            self.conTab.setLayout(gridLay)

    #Vista para ver la interfaz requerida para el vuelo autónomo
    def auTab(self):
        gridLay = QGridLayout()
        opciones = ['', 'label', '' , '', 'Button', '', '', '', '']
        position = [(i, j) for i in range(3) for j in range(3)]

        for position, opcion in zip(position, opciones):
            if opcion == '':
                vacio = QLabel(self)
                vacio.setText('')
                gridLay.addWidget(vacio, *position)
            if opcion == 'label':
                self.letreroEstado = QLabel(self)
                self.letreroEstado.setText('Oprima para iniciar el vuelo')
                self.letreroEstado.setAlignment(Qt.AlignCenter)
                self.letreroEstado.setStyleSheet('font-size: 18px;')
                gridLay.addWidget(self.letreroEstado, *position)
            if opcion == 'Button':
                self.autoButton = QPushButton('Iniciar vuelo')
                self.autoButton.setFixedHeight(60)
                self.autoButton.setFixedWidth(250)
                self.autoButton.setStyleSheet('background-color: #00C853')
                self.autoButton.clicked.connect(self.checkingButton)
                gridLay.addWidget(self.autoButton, *position)

        self.autoTab.setLayout(gridLay)


    def envioAuto(self):
        self.autoButton.setStyleSheet('background-color: #DD2C00')
        self.autoButton.setText('Abortar Vuelo')
        self.letreroEstado.setText('Presiona para abortar vuelo')

    def abortAuto(self):
        self.autoButton.setText('Vuelo abortado')
        self.letreroEstado.setText('Drone regresando a la base')
        time.sleep(3)
        print('cambio')
        #self.autoButton.setText('Inciar Vuelo')
        #self.letreroEstado.setText('Oprima para iniciar vuelo')
        #self.autoButton.setStyleSheet('background-color: #00C853')

    def Menubar(self):
        #Creating the MenuBar
        menubar = self.menuBar()
        fileF = menubar.addMenu("&Archivo")
        edit = menubar.addMenu("&Editar")
        view = menubar.addMenu("&Ver")
        toolMenu = menubar.addMenu("&Herramientas")
        helpMenu = menubar.addMenu("&Ayuda")

        #Add actions to the menu bar
        fileF.addAction(self.helpAction)
        helpMenu.addAction(self.helpAction)

    def checkingButton(self):
        self.autoState = not self.autoState
        if self.autoState:
            self.envioAuto()
        else:
            self.abortAuto()

    def InitUi(self):
        #Método que inicializa la interfaz
        self.Toolbar()
        self.Menubar()
        self.tabWid()
        self.forecastTable()
        self.controlTab()
        self.auTab()
        self.setWindowTitle('Andra')
        self.setCentralWidget(self.tabs)
        self.setGeometry(100,100,900,600)
        self.autoState = False


def main():
    app =QApplication(sys.argv)
    main_window = Main()
    main_window.show()
    app.exec_()


if __name__ == "__main__":
    main()