# -*- coding: utf-8 -*-
#Librería necesaria para usar los puertos gpio de la raspbery pi
import RPi.GPIO as GPIO
#Importa el objeto NRF24 para poder uusar el receptor de radio frecuencias
from lib_nrf24 import NRF24
import time
import spidev
#La libería necesaria para usa sqlite
import sqlite3 as lite

#Creamos o nos conectamos con una base datos en la
db = lite.connect('mydatabase.db')

#Creamos el objeto cursor para poder llevar a cabo cualquier
#operación con la base de datos
cursor = db.cursor()

#Creamos la base de datos si no existe
cursor.execute('''CREATE TABLE IF NOT EXISTS clima( tdate DATE, ttime TIME, tempertura REAL, Hum REAL, CO2 REAL, absPress REAL, altitud REAL, seaLevelPress REAL, zona TEXT)''')
#Usamos el sistema de numeración broadcom
GPIO.setmode(GPIO.BCM)
#Declaramos los canales de comunicación
pipes = [[0xE8, 0xE8, 0xF0, 0xF0, 0xE1], [0xF0, 0xF0, 0xF0, 0xF0, 0xE1]]

#Instanciamos un objeto de la clase NRF24
radio = NRF24(GPIO, spidev.SpiDev())

#inicializamos la radio como se especifica en la documentación de la librería
radio.begin(0, 17)

#Configuración necesaria para el sensor
radio.setPayloadSize(32)
radio.setChannel(0x76)
radio.setDataRate(NRF24.BR_1MBPS)
radio.setPALevel(NRF24.PA_MIN)

radio.setAutoAck(True)
radio.enableDynamicPayloads()
radio.enableAckPayload()

radio.openReadingPipe(1, pipes[1])

radio.printDetails()
radio.startListening()


class ReceptorArduino:
    #Variable usadas para guardar valores en la base de datos
    tempDB = ""
    humDB = ""
    CO2DB = ""
    absPressDB = ""
    altDB = ""
    seaLevelPressDB = ""
    zonaActualDB = "Puebla, Pue. MX"

    #Banderas usadas para poder guardar el valor de las varibles
    #anteriores una vez que hayan guardado datos procedetes del arduino
    tempBool = False
    humBool = False
    CO2Bool = False
    absPressBool = False
    altBool = False
    seaLevelBool = False

    def __init__(self):
        pass

    def guardarDB(self):
        if self.tempBool and self.humBool and self.CO2Bool and self.absPressBool and self.altBool and self.seaLevelBool:
            global db
            db = lite.connect('mydatabase.db')
            cursor = db.cursor()
            cursor.execute("""INSERT INTO clima values(date('now'), time('now'), (?), (?), (?), (?), (?), (?), (?))""", (self.tempDB, self.humDB, self.CO2DB, self.absPressDB, self.altDB, self.seaLevelPressDB, self.zonaActualDB))
            db.commit()
            db.close()
            self.tempBool = False
            self.humBool = False
            self.CO2Bool = False
            self.absPressBool = False
            self.altBool = False
            self.seaLevelBool = False

    def medicion(self):
        while True:

            while not radio.available(0):
                time.sleep(100 / 1000)

            receiveData = ["0000000"]
            recv_buffer = []

            radio.read(recv_buffer, radio.getDynamicPayloadSize())
            #print("Recived {}".format(recv_buffer))
            receiveData[0] = ''.join(chr(o) for o in recv_buffer)
            #print(receiveData[0])
            receiveData[0] = list(receiveData[0])
            #print(receiveData[0])
            #print (len(receiveData[0]))
            count = 0
            #cuenta cuantas veces aparece '\x00' en el arreglo recibido
            for i in range(0, len(receiveData[0])):
                if receiveData[0][i] == '\x00':
                    count += 1
            #Ejecuta la función remove sobre el
            #arreglo el mismo número de veces que aparece '\x00'
            for e in range(count):
                receiveData[0].remove('\x00')
            #Temperatura
            if receiveData[0][-1] == 'T':
                self.tempDB = self.switch(receiveData[0])
                self.tempBool = True
            #humedad
            if receiveData[0][-1] == 'H':
                self.humDB = self.switch(receiveData[0])
                self.humBool = True
            #CO2
            if receiveData[0][-1] == 'C':
                self.CO2DB = self.switch(receiveData[0])
                self.CO2Bool = True
            #Presión absoluta
            if  receiveData[0][-1] == 'P':
                self.absPressDB = self.switch(receiveData[0])
                self.absPressBool = True
            #Altitud
            if receiveData[0][-1] == 'M':
                self.altDB = self.switch(receiveData[0])
                self.altBool = True
            #Presión ajustada a nivel de mar
            if receiveData[0][-1] == 'S':
                self.seaLevelPressDB = self.switch(receiveData[0])
                self.seaLevelBool = True

            #print(receiveData[0])
            #print(" al final")

            self.guardarDB()

    def switch(self, datosRecibidos):
        #Función que refactoriza lo que se hacen las condicionales
        print(datosRecibidos[0])
        #Elimina la letra que india que tipo d magnitud es
        datosRecibidos[0].pop()
        #Convierte la lista en un string
        datosRecibidos[0] = ''.join(datosRecibidos[0])
        #Convierte el string en flotante para ser guardado en la base de datos
        return float(datosRecibidos[0])