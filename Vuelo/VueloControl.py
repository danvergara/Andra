# -*- coding: utf-8 -*-
#Xbox 360 controller library
import xbox
import time
from pygame import *
import pygame
import libardrone #Ar-drone 2.0 control library
import threading
import time
from subprocess import call

import signal
import sys
#intance an ARDrone object
drone = libardrone.ARDrone()


#setup limit variable
limit = 0.35

# Setup joystick
joy = xbox.Joystick()

try:
    #Valid connect may require joystick input to occur
    print "Waiting for Joystick to connect"
    while not joy.connected():
        time.sleep(0.10)

    #Show misc inputs until Back button is
    while not joy.Back() and joy.connected():
        if joy.A(): #When press A button the drone takes off
            print "takeoff"
            drone.takeoff()
        elif joy.B(): #When press B button the drone shakes
            print "disable Emergency"
            drone.reset()
        elif joy.X(): #When press X button the drone lands
            print "land"
            drone.land()



       #Joystick movement
       #The drone moves up
        if joy.leftY() > limit:
            print "altitud up"
            drone.speed = 3
            drone.move_up()
        #The drone moves down
        elif joy.leftY() < -limit:
            print "altitud down"
            drone.speed = 2
            drone.move_down()
        #Turns right
        elif joy.leftX() > limit:
            print "turn right"
            drone.speed = 1
            drone.turn_right()
        #Turns left
        elif joy.leftX() < -limit:
            print "turn left"
            drone.speed = 1
            drone.turn_left()
        #Moves forward
        elif joy.rightY() > limit:
            print "move fordward"
            drone.speed = 1
            drone.move_forward()
        #Moves backward
        elif joy.rightY() < -limit:
            print "move backward"
            drone.speed = 1
            drone.move_backward()
        #Moves right
        elif joy.rightX() > limit:
            print "move right"
            drone.speed = 1
            drone.move_right()
        #Moves left
        elif joy.rightX() < -limit:
            print "move left"
            drone.speed = 1
            drone.move_left()

finally:
    #Always close out so that xboxdrv subprocess ends
    drone.reset()
    drone.halt()
    joy.close()
    print "Done."


